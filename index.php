<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>СПбКИТ: Виртуальная лаборатория</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>
<?
  	session_start();
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true) {
		header("Location: lab.php");
	}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    СПбКИТ
                    <small>Виртуальная лаборатория</small>
                </h1>
            </div>
            <h3 class="text-center" style="margin-bottom: 150px; margin-top: 50px;">
                Авторизируйтесь для работы с лабораторией
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-inline" role="form" method="post" action="action.php?do=login">
                        <div class="form-group col-md-offset-3">
                            <label for="exampleInputEmail1">
                                Логин
                            </label>
                            <input class="form-control" name="login" type="text">
                            <label for="password">
                                Пароль
                            </label>
                            <input class="form-control" name="password" type="password">
                            <input type="submit" class="btn btn-default" value="LOGIN">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 30%;">
                Copyright © , 2007 Санкт-Петербургский колледж информационных технологий.
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/login_scripts.js"></script>
</body>
</html>
