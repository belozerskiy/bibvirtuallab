<?
	require 'remote.php';
	function vmstate($vmname) {
		$vms=cluster_exec('virsh list --all');
		$match = "/" . $vmname . " .*running/";
		if (preg_match($match, $vms)) {
			$state = "on";
		} else {
			$state = "off";
		}
		return $vmname . " state: " . $state . "<br>";
	}
	function create_snapshot($vmname) {
		$snapshot_info=cluster_exec('virsh snapshot-create ' . $vmname);
		return $snapshot_info;
	}
?>
