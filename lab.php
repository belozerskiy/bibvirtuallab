<?
	require_once('database.php');
	function populate_checkin($date, $time, $login) {
		$res=mysql_query("SELECT * FROM lab_sessions WHERE date='$date' AND time='$time' AND user_login=''");
		$count=mysql_query("SELECT * FROM lab_sessions WHERE date='$date' AND user_login='$login'");
		if (array_shift(explode(':', date('H:i')))<$time || $date>date('Y-m-d')) {
			if (mysql_num_rows($res)==1) {
				if (mysql_num_rows($count)<=1) {
					$check_get_url="'action.php?do=checkin'";
					$date="'" . $date . "'";
					$time="'" . $time . "'";
					echo '<button class="btn btn-default" onclick="$.post(' . $check_get_url . ', { time: ' . $time . ', date: ' . $date . ' }, function(data) { location.reload(); });">Participate</button>';
				} else { echo 'No limit';  }
			} else {
				$free=mysql_query("SELECT * FROM lab_sessions WHERE date='$date' AND time='$time' AND user_login='$login'");
				if (mysql_num_rows($free)==0) {
					echo 'Used';
				} else {
					echo 'Used by you';
				}
			}
		} else { echo 'Time expired';  }
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>СПбКИТ: Виртуальная лаборатория</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<?
	date_default_timezone_set("Europe/Moscow");
	session_start();
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true) {
		$date=date('Y-m-d');
		$time=array_shift(explode(':', date('H:i')));
		$login=$_SESSION['username'];
        $sess_time=mysql_result(mysql_query("SELECT time FROM lab_sessions where date = '$date' and user_login='$login' order by time desc limit 1"), 0).":00";
        $sess_time_limit=array_shift(explode(':', date('H:i',strtotime($sess_time)+60*60)));
        $sess_time=array_shift(explode(':', $sess_time));
		$tomorrow_date=date('Y-m-d', strtotime(($date . '+1 days')));
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    СПбКИТ
                    <small>Виртуальная лаборатория</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 text-center">
            <img id="profile-img" src="user.png" style="margin: 0 auto;" class="img-responsive"><br>
            <p class="text-center"><? echo $_SESSION['username']; ?></p><br>
		<p id="vncpass" class="text-center"><? echo "VNC pass:" . $_SESSION['vncpass']; ?></p><br>	
		<p id="remain_time" class="text-center"></p>
            <button type="button" class="btn btn-default" style="margin-top: 230px;" onclick="document.location.href='action.php?do=logout';">
                Выйти
            </button>
        </div>
	<?
		if ($time==$sess_time || $time==$sess_time_limit) {
		$_SESSION['vncpass']=mysql_result(mysql_query("SELECT vnc_pass FROM lab_sessions WHERE date = '$date' AND time = '$sess_time' AND user_login='$login'"), 0);
	?>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
            </div>
        </div>
    </div>
	<?
		} else {
	?>
        <div class="col-md-10">
            <h3>
                Расписание виртуальной лаборатории:
            </h3>
            <br>
            <h4>
                Сегодня:
            </h4>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Дата
                    </th>
                    <th>
                        Время
                    </th>
                    <th>
                        Запись
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <? echo $date;  ?>
                    </td>
                    <td>
                        9.00-11.00
                    </td>
                    <td>
                        <? populate_checkin($date, '09', $login); ?>
                    </td>
                </tr>
                <tr class="active">
                    <td>
                        <? echo $date;  ?>
                    </td>
                    <td>
                        11.00-13.00
                    </td>
                    <td>
                        <? populate_checkin($date, '11', $login); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <? echo $date;  ?>
                    </td>
                    <td>
                        13.00-15.00
                    </td>
                    <td>
                        <? populate_checkin($date, '13', $login); ?>
                    </td>
                </tr>
                <tr class="active">
                    <td>
                        <? echo $date;  ?>
                    </td>
                    <td>
                        15.00-17.00
                    </td>
                    <td>
                        <? populate_checkin($date, '15', $login); ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <h4>
                Завтра:
            </h4>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Дата
                    </th>
                    <th>
                        Время
                    </th>
                    <th>
                        Запись
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <? echo $tomorrow_date;  ?>
                    </td>
                    <td>
                        9.00-11.00
                    </td>
                    <td>
                        <? populate_checkin($tomorrow_date, '09', $login); ?>
                    </td>
                </tr>
                <tr class="active">
                    <td>
                        <? echo $tomorrow_date;  ?>
                    </td>
                    <td>
                        11.00-13.00
                    </td>
                    <td>
                        <? populate_checkin($tomorrow_date, '11', $login); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <? echo $tomorrow_date;  ?>
                    </td>
                    <td>
                       13.00-15.00
                    </td>
                    <td>
                        <? populate_checkin($tomorrow_date, '13', $login); ?>
                    </td>
                </tr>
                <tr class="active">
                    <td>
                        <? echo $tomorrow_date;  ?>
                    </td>
                    <td>
                        15.00-17.00
                    </td>
                    <td>
                        <? populate_checkin($tomorrow_date, '15', $login); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
	<?
		}
	?>
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%;">
            Copyright © , 2007 Санкт-Петербургский колледж информационных технологий.
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/lab_scripts.js"></script>
<?
	} else {
	header ("Location: index.php");
	}
?>
</body>
</html>
