<?
	require_once('database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>СПбКИТ: Виртуальная лаборатория</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<?
	date_default_timezone_set("Europe/Moscow");
	session_start();
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true) {
	$date=date('Y-m-d');
	$time=date('H');
	$login=$_SESSION['username'];
	$res=mysql_query("SELECT * FROM lab_sessions WHERE date = '$date' AND time = '$time' AND user_login='$login'");
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    СПбКИТ
                    <small>Виртуальная лаборатория</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 text-center">
            <img id="profile-img" src="user.png" style="margin: 0 auto;" class="img-responsive"><br>
            <p class="text-center"><? echo $_SESSION['username'] ?></p>
            <button type="button" class="btn btn-default" style="margin-top: 230px;" onclick="document.location.href='action.php?do=logout';">
                Выйти
            </button>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
                <div class="col-md-4 text-center">
                    <img src="pc.png" style="margin: 0 auto;" class="img-responsive"><br>
                    <button type="button" class="btn btn-default" style="margin: 10px;">
                        Подключиться
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%;">
            Copyright © , 2007 Санкт-Петербургский колледж информационных технологий.
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/lab_scripts.js"></script>
<?
	} else {
	header ("Location: index.php");
	}
?>
</body>
</html>
