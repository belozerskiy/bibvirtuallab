<!DOCTYPE html>
<html> <!--!>.main>(.menu>li*4)+(.content>table>tr(td{cjcb vj. gbc.}*10)*5) -->
	<head>
		<meta charset="utf8">
		<title>Компания "<?PHP	 	 echo $companyname; ?>"</title>
		<link rel="stylesheet" href="bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<div class="main">
		<header><?=$companyname?></header>
		<div class="menu">
		<center><table>
			<tr>
<?=$menu?>
			</tr>
		</table></center>
		</div>
			<div class="container">
                <?=$content?>
			</div>
			<footer>
			<p>&copy; <?=$companyname?> 2016</p>
			</footer>
		</div>
	</body>
</html>
